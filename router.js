import todocard from './component/todocard.js';
import tododata from './component/tododata.js';


export default 
[
    {
        path:'/',
        component:todocard
    },
    {
        path:'/tododata/:title',
        component:tododata
    }
]
