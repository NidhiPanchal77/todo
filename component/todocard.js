export default {
    name:"todotitle",
    props:["todos","todo_title"],
    data(){
        return{
            task:"",
            taskenter:''
        }
    },
    methods:{
        onEnter() { 
            this.taskenter = this.task;
            this.todos.push({title:this.taskenter});
            axios.post('http://localhost:3000/todo',{
            title:this.taskenter
        },{
                headers :{
                    'Content-Type' : 'application/json',
                }
            }).then((resp) =>{
                this.task = ""
            }).catch((error) => {console.log(error)})
        },

        childevent: function(index){
            //console.log("thuis is child event"+index);
                this.todos.splice(index,1)
                console.log(this.todos[index].title)
            }
    },
    template:`
                <div class="card">
                    <nav>
                        <span class="title lead">TODO LIST</span>
                    </nav>
                    
                    <div class="inputel">
                        
                            <input type="text" placeholder="Enter task here..." v-model="task" @keyup.enter="onEnter" />
                        
                    </div>

                    <div class="card-body">
                        <li v-for="(todo_title,index) in todos" :key="todo_title.id"><router-link :to="'/tododata/'+todo_title.title">{{todo_title.title}}</router-link><button class="btn btn-sm btn-danger" @click="childevent(index)">X</button></li>
                    </div>
                    
                </div>
    `
}