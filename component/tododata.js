///localcomponent for li display
var nav_bar = {
    template:`
    <nav>
        <span class="title lead"><slot></slot></span>
    </nav>
    `
};


var list_item  = {
    data(){
        return{
            
        }
    },
    props:["item","data",'ti'],
    methods:{
        childclk: function(index){
            this.$emit('childevent',index);
        }
    },
    template:`
    <ul>
        <li v-for="(item,index) in data.list"><input type="checkbox" id="checkbox" :checked="item.check">{{item.item}}</input><button class="btn btn-sm btn-danger" v-on:click="childclk(index)">X</button></li>
    </ul>
    `
};

//parent component
export default {
    data(){
        return{
            data_fet : [], 
            data_try:[],
            data_del:[],
            task:'',
            taskenter:''
        }
    },
    components:{
        "list-item":list_item,
        "nav-title":nav_bar
    },
    props:["todo_title"],
    created(){
        var data_fetched = this.$route.params.title;
        var _this = this;
        axios.get("http://localhost:3000/todo?title="+data_fetched)
        .then(function(response){
            _this.data_fet = response.data[0];    
        }).catch(function(error){
            console.log(error)
        })
    },
    methods:{
        childevent: function(index){
            //console.log("thuis is child event"+index);
            this.data_fet.list.splice(index,1);
                console.log(index)
            },

            backbtn: function(){
                return this.$router.go(-1);
            },

            onEnter() { 
                this.taskenter = this.task;
                this.data.list.push({item:this.taskenter});
                axios.post('http://localhost:3000/todo',{
                item:this.data
            },{
                    headers :{
                        'Content-Type' : 'application/json',
                    }
                }).then((resp) =>{
                    this.task = ""
                }).catch((error) => {console.log(error)})
            },
    },
    template:`
        <div class="card">
            <nav-title><button class="btn btn-sm btn-warning" @click="backbtn()">Back</button>{{data_fet.title}}</nav-title>
            <input type="text" placeholder="Enter task here..." v-model="task"  />
            <div class="card-body">
                <list-item :data="data_fet" v-on:childevent="childevent"></list-item>
            </div>
        </div>
    `
}