import todocard from './component/todocard.js';
import routes from './router.js';

Vue.use(VueRouter);

const router = new VueRouter({
    routes:routes
});

var app = new Vue({
    el:"#app",
    router,
    data(){
        return{
            todos:[],
            todo_title:[] 
        }
    },
    components:{
        'todo-card':todocard
    },
    created(){
        var _this = this;
        axios.get("http://localhost:3000/todo")
        .then(function(resp) {
            _this.todos = resp.data;
        }).catch(function(error){
            console.log(error)
        })
    }
})